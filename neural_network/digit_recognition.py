#digit recognition using a deep belief neural network using nolearn package and sklearn
#The mnist dataset is used for the training and testing

##Importing necessary packages
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
from sklearn import datasets
from nolearn.dbn import DBN 
import numpy as np 
import cv2


#Grab the Mnist dataset (55mb)
dataset=datasets.fetch_mldata("MNIST original")
print "[X] downloading ......"

#scale data to range (0,1) then split into training and testing sets

(trainX,testX,trainY,testY)=train_test_split(
	dataset.data/255.0,dataset.target.astype('int0'),test_size=0.33)

#Train the deep belief network with 784 input units
#28 by 28 grayscale image 300 hidden units and 10 output units i.e digits (0-9)
dbn=DBN(
[trainX.shape[1],300,10],
learn_rates=0.3,
learn_rate_decays=0.9,
epochs=10,
verbose=1
	
)
dbn.fit(trainX,trainY)
#compute predictions for the test set
pred=dbn.predict(testX)
print classification_report(testY,pred)

for i in np.random.choice(np.arange(0,len(testY)),size=(10,)):
	#classify the digit
	pred=dbn.predict(np.atleast_2d(testX[i]))

	#reshape the feature vector to 28 by 28 then change the format to unint8
	image=(testX[i]*255.0).reshape(28,28).astype("uint8")

	#show the image
	print "Actual digit is {0}, predicted digit is {1}".format(testY[i],pred[0])
	cv2.imshow("Digit",image)
	cv2.waitKey(0)
